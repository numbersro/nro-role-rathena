from __future__ import absolute_import
from ansible.plugins.callback import CallbackBase
import json
from datetime import datetime

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

import logging
# logging.basicConfig(level=logging.DEBUG)

import os
from slack import WebClient
from slack.errors import SlackApiError
import hashlib


VV_FIREBASE_NRF5340_DB_URL = 'https://vv-stack-example-default-rtdb.firebaseio.com/'
GOOGLE_SERVICE_ACCOUNT_CERT_PATH = "vv-stack-example-firebase-adminsdk-ugpdk-87060c86a1.json"


# init db client to push new data
cred = credentials.Certificate( GOOGLE_SERVICE_ACCOUNT_CERT_PATH )
firebase_admin.initialize_app(cred,  {'databaseURL': VV_FIREBASE_NRF5340_DB_URL})

slack_token = "xoxb-727586316899-2054576306738-G1pJJwWPOcW47X97kTVt1HjB"
ref = db.reference("example_host_name")
sha_1 = hashlib.sha1()
#client = WebClient(token=slack_token)



'''

 (!) 5/11/2021 does not handle FATAL errors

'''



# ref = db.reference('this_host_name')


class CallbackModule(CallbackBase):

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'json_cb'

    def __init__(self):
        self.tasks = {}
        self.timeobj = now = datetime.now()

# ,result=result._result
    def dump_result(self, result, ignore_errors=False):
        pass
        #print(json.dumps(dict(name=self.tasks[result._task._uuid]), indent=2))

        #print(json.dumps(dict(name=self.tasks[result._task._uuid],result=result._result), indent=2))

    def v2_playbook_on_task_start(self, task, is_conditional):
        self.tasks[task._uuid] = task.name
        #print("DEBUG_HERE_@@@: {}".format(task.role))
        current_time = self.timeobj.strftime("%m/%d/%y %H:%M:%S")
        print("{} -- {}".format( current_time, task ))
        sha_1.update(bytes(task.name, 'ascii'))
        print("hex={}".format(sha_1.hexdigest()))
        task_hash_id_string = sha_1.hexdigest()
        ref.child("playbook").child(task_hash_id_string).set({"full_task_string": str(task),  "task_name_string": str(task.name), "last_updated_on" : current_time, "result" : { "test" : "data123" } })
        # try: 
        #   response = client.chat_postMessage(
        #     channel="test",
        #     text=json.dumps({host.name: result._result}, indent=4)
        #   )
        # except SlackApiError as e:
        #   # You will get a SlackApiError if "ok" is False
        #   assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'

    def v2_runner_on_failed(self, result, ignore_errors=False):
        payload = {'state': 'failed',
                   'task_name': result.task_name,
                   'task_output' : result._result['msg']
                  }

        print('On a failed task - Sending to endpoint:\n{0}\n'
              .format("yourbutt"))
        pass

    def v2_runner_on_ok(self, result):
        """Print a json representation of the result.

        Also, store the result in an instance attribute for retrieval later
        """
        host = result._host
        self.host_ok[host.get_name()] = result
        print("@@@@@@@@@@@@@@@@@@@@@@")
        #print(json.dumps({host.name: result._result}, indent=4))



    def v2_runner_on_unreachable(self, result):
        host = result._host
        self.host_unreachable[host.get_name()] = result

    def v2_playbook_on_stats(self, stats):
        hosts = sorted(stats.processed.keys())
        print(dir(stats))
        print(stats.__dict__)
        for host in hosts:
            print(stats.summarize(host))


    v2_runner_on_ok = dump_result
    v2_runner_on_failed = dump_result