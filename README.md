Role Name
=========

Should install the latest `stable` version of `Hercules.ws` on linux debian host.

Example of `playbook` utilizing `nu-role-hercules`:
`Requires: athena_server_role` `hostvar` defined for the three containers equivalent to `login` `char` and `map`. avaialble definition are: `login` `char` `map`

```
---
- name: example playbook 
  hosts: all

  pre_tasks:
    - name: "add login char map serve to inventory group"
      add_host:
        hostname: "{{ remote_lxd_addr }}:{{ item }}"
        groups:
          - "test_group_molecule_nu_role_hercules"
        ansible_connection: lxd
        ansible_python_interpreter: /usr/bin/python3
      with_items:
        - "us-numbersro-login"
        - "us-numbersro-char"
        - "us-numbersro-map"

    # - command: "echo hello world" doesnt work since container not created yet :)
    #   delegate_to: "{{ remote_lxd_addr }}:us-numbersro-login"

  roles:
    # - name: init the hercules stack on new molecule container
    #   role: vv-role-lxd-containers 
    #   vars:   
    #     vv_lxd_stacks: 
    #      - stack_project_id: 24821010
    #        stack_ref_name: master

    # we want this to be with auto playbook

:
    - name: apply the nu-role-hercules login to install and configure hercules to containers
      role: nu-role-hercules
      delegate_to: "{{ remote_lxd_addr }}:us-numbersro-login"
      vars:
        athena_server_role: login

    - name: apply the nu-role-hercules char to install and configure hercules to containers
      role: nu-role-hercules
      delegate_to: "{{ remote_lxd_addr }}:us-numbersro-char"
      vars:
        athena_server_role: char

    - name: apply the nu-role-hercules char to install and configure hercules to containers
      role: nu-role-hercules
      delegate_to: "{{ remote_lxd_addr }}:us-numbersro-map"
      vars:
        athena_server_role: map
  vars:
    remote_lxd_addr: "molecule--nu-ansible-hercules"
    vv_lxd_bridge_name: nested_lxd_br0
```



Requirements
------------

Ror each Hercules container or host, `athena_server_role:` `login|char|map`

Example in inventory file:
```
[hercules_hosts]
some_data_center ansible_host=22.23.22.21 ansible_user=ubuntu athena_server_role=login
some_data_center ansible_host=22.23.22.22 ansible_user=ubuntu athena_server_role=char
some_data_center ansible_host=22.23.22.23 ansible_user=ubuntu athena_server_role=map
```


Role Variables
--------------

```
---
# defaults file for nr
nu_athena_directory_path: /opt/athena
nu_athena_linux_default_user: ubuntu
nu_athena_source_zip_url: https://github.com/HerculesWS/Hercules/archive/stable.zip
nu_public_ip_addr: "51.81.33.192"
nu_athena_hercules_stable_zip_tmp_path: /tmp/hercules.zip

```

Dependencies
------------

`pip3 ansible molecule`


`Noticeable to do's:` best to allow user to define login char and map container ip. right now its hardcoded 11.0.0.0|1|2|3 . #matrix
------------
 
```
```


License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
